# Girderstream source plugin

Plugins to fetch source code from tar or git sources.

## Build for musl

To build the plugins for musl

```bash
rustup target add x86_64-unknown-linux-musl
cargo build --release --target x86_64-unknown-linux-musl
cp target/x86_64-unknown-linux-musl/release/plugin_git target/x86_64-unknown-linux-musl/release/plugin_tar output/
casupload --cas-server=http://localhost:50040 output
```
