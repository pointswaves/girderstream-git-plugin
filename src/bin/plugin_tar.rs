use std::{path::Path, process::Stdio};

use anyhow;
use clap::{self, Parser};
use tokio::{fs, io::AsyncWriteExt, process::Command};

/// Simple program to create git commands
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// The sha of the tar
    #[clap(short, long)]
    sha: String,
    /// The url of the tar to fetch
    #[clap(short, long)]
    url: String,
    /// The dir inside the tar to use
    #[clap(long)]
    unpack: Option<String>,
    /// Checkout to a subdir of /source/
    #[clap(long)]
    stage_dir: Option<String>,
}

pub type Crapshoot = anyhow::Result<()>;

#[tokio::main]
async fn main() -> Crapshoot {
    real_main().await
}

async fn real_main() -> Crapshoot {
    let args = Args::parse();

    fs::create_dir_all("/source/").await?;

    // mkdir -p sources/ output_dir/ /etc/; cd /sources
    // echo "nameserver 8.8.8.8" > "/etc/resolv.conf"
    // wget https://gitlab.com/pointswaves/girderstream-test-project/-/archive/main/girderstream-test-project-main.tar.gz --no-check-certificate
    // sha256sum girderstream-test-project-main.tar.gz
    // echo "73e494ce434108c1c2f3a3e6914297c7c78d3c8d2a4d93b8cb389dc5921e4877 girderstream-test-project-main.tar.gz" | sha256sum --check
    // tar -xf girderstream-test-project-main.tar.gz
    // mv girderstream-test-project-main/* /output_dir/

    // mkdir -p sources/ output_dir/; cd /sources
    // echo "nameserver 8.8.8.8" > "/etc/resolv.conf"
    // wget https://ftp.gnu.org/pub/gnu/ncurses/ncurses-6.2.tar.gz
    // tar -xf ncurses-6.2.tar.gz --no-same-owner
    // mv ncurses-6.2/* /output_dir/

    let final_bit = args.url.clone().split('/').last().unwrap().to_string();

    let result = Command::new("wget")
        .current_dir("/source/")
        .arg(args.url)
        // todo option to not skip
        .arg("--no-check-certificate")
        .spawn()
        .expect("wget command failed to start")
        .wait()
        .await
        .expect("failed to execute process");
    if !result.success() {
        anyhow::bail!("wget did not complete successfully")
    }

    Command::new("sha256sum")
        .current_dir("/source/")
        .arg(final_bit.clone())
        .spawn()
        .expect("sha256sum command failed to start")
        .wait()
        .await
        .expect("failed to execute process");
    // This just puts the actual sha in to stdout to help with debug or tracking

    let mut sha_cmd = Command::new("sha256sum")
        .current_dir("/source/")
        .arg("--check")
        .stdin(Stdio::piped())
        .spawn()
        .unwrap();

    let mut stdin = sha_cmd
        .stdin
        .take()
        .expect("child did not have a handle to stdin");
    stdin
        .write_all(format!("{} {final_bit}\n", args.sha).as_bytes())
        .await
        .unwrap();
    stdin.flush().await?;
    stdin.shutdown().await?;
    drop(stdin);

    println!("Waiting for sha256sum to complete");

    let result = sha_cmd.wait().await.unwrap();
    if !result.success() {
        anyhow::bail!("sha256sum did not complete successfully")
    }

    println!("Starting extraction");

    let output_dir = if let Some(stage_to) = args.stage_dir {
        format!("/output_dir/{}/", stage_to)
    } else {
        "/output_dir/".to_string()
    };
    let output_dir_path = Path::new(&output_dir);
    let parent = output_dir_path
        .parent()
        .unwrap_or_else(|| panic!("The output_dir path {output_dir} must have a parent"));
    fs::create_dir_all(parent).await?;

    let extract_to: String = if args.unpack.is_some() {
        "/extract_dir/".to_string()
    } else {
        output_dir.clone()
    };
    fs::create_dir_all(&extract_to).await?;

    let result = Command::new("tar")
        .current_dir("/source/")
        .arg("-xf")
        .arg(final_bit)
        .arg("--no-same-owner")
        .arg("-C")
        .arg(extract_to.clone())
        .arg("--checkpoint=.100")
        .spawn()
        .expect("tar command failed to start")
        .wait()
        .await
        .expect("failed to execute process");
    if !result.success() {
        anyhow::bail!("tar did not complete successfully")
    }

    if let Some(extract_dir) = args.unpack {
        println!("Moving files to output_dir");
        let mut command = Command::new("mv");
        command
            .current_dir("/source/")
            .arg(format!("/{extract_to}/{extract_dir}/"))
            .arg(output_dir);
        println!("mv {:?}", command);
        let result = command
            .spawn()
            .expect("mv command failed to start")
            .wait()
            .await
            .expect("failed to execute process");
        if !result.success() {
            anyhow::bail!("mv did not complete successfully")
        }
    }

    Ok(())
}
