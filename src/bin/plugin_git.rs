use std::process::Stdio;

use anyhow;
use clap::{self, Parser};
use tokio::{
    fs::{self, File},
    io::{AsyncReadExt, AsyncWriteExt},
    process::Command,
};

/// Simple program to create git commands
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Name of the sha to checkout
    #[clap(short, long)]
    sha: String,
    /// Name of the url to pull from
    #[clap(short, long)]
    url: String,
    /// Name of the thing to fetch
    #[clap(short, long)]
    fetch: Option<String>,
    /// Name of the thing to fetch
    #[clap(short, long)]
    track_ref: Option<String>,

    /// Perform tracking action
    #[clap(short, long, action)]
    track: bool,
}

pub type Crapshoot = anyhow::Result<()>;

#[tokio::main]
async fn main() -> Crapshoot {
    let args = Args::parse();
    fs::create_dir_all("/output_dir/").await?;

    // git init
    // git remote add origin <url>

    let result = Command::new("git")
        .current_dir("/output_dir/")
        .arg("init")
        .spawn()
        .expect("git command failed to start")
        .wait()
        .await
        .expect("failed to execute process");
    if !result.success() {
        anyhow::bail!("Failure running fetch")
    };

    let result = Command::new("git")
        .current_dir("/output_dir/")
        .arg("remote")
        .arg("add")
        .arg("origin")
        .arg(args.url)
        .spawn()
        .expect("git command failed to start")
        .wait()
        .await
        .expect("failed to execute process");
    if !result.success() {
        anyhow::bail!("Failure running fetch")
    };

    if args.track {
        let result = Command::new("git")
            .current_dir("/output_dir/")
            .arg("fetch")
            .arg("origin")
            .spawn()
            .expect("git command failed to start")
            .wait()
            .await
            .expect("failed to execute process");
        if !result.success() {
            anyhow::bail!("Failure running fetch")
        };

        let mut command = Command::new("git")
            .current_dir("/output_dir/")
            .arg("rev-parse")
            .arg("--verify")
            .arg(format!("origin/{}", args.track_ref.unwrap_or("master".to_string())))
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()
            .expect("git command failed to start");

        let result = command.wait().await.expect("failed to execute process");
        if !result.success() {
            anyhow::bail!("Failure running fetch")
        };
        let mut new_sha = String::new();
        command.stdout.unwrap().read_to_string(&mut new_sha).await?;

        let mut file = File::create("/output_dir/tracked.yml").await?;
        file.write_all(format!("sha: {}", new_sha).as_bytes())
            .await?;
    } else {
        // git fetch --depth 1 origin <sha1>
        // git checkout FETCH_HEAD

        if let Some(fetch) = args.fetch {
            let result = Command::new("git")
                .current_dir("/output_dir/")
                .arg("fetch")
                .arg("origin")
                .arg(fetch)
                .spawn()
                .expect("git command failed to start")
                .wait()
                .await
                .expect("failed to execute process");
            if !result.success() {
                anyhow::bail!("Failure running fetch")
            };

            let result = Command::new("git")
                .current_dir("/output_dir/")
                .arg("checkout")
                .arg(args.sha)
                .spawn()
                .expect("git command failed to start")
                .wait()
                .await
                .expect("failed to execute process");
            if !result.success() {
                anyhow::bail!("Failure running fetch")
            };
        } else {
            let result = Command::new("git")
                .current_dir("/output_dir/")
                .arg("fetch")
                .arg("--depth")
                .arg("1")
                .arg("origin")
                .arg(args.sha)
                .spawn()
                .expect("git command failed to start")
                .wait()
                .await
                .expect("failed to execute process");
            if !result.success() {
                anyhow::bail!("Failure running fetch")
            };

            let result = Command::new("git")
                .current_dir("/output_dir/")
                .arg("checkout")
                .arg("FETCH_HEAD")
                .spawn()
                .expect("git command failed to start")
                .wait()
                .await
                .expect("failed to execute process");
            if !result.success() {
                anyhow::bail!("Failure running fetch")
            }
        }
    }

    Ok(())
}
